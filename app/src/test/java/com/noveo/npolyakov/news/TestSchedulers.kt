package com.noveo.npolyakov.news

import com.noveo.npolyakov.news.model.system.SchedulersProvider
import io.reactivex.schedulers.Schedulers


class TestSchedulers : SchedulersProvider {
    override fun ui() = Schedulers.trampoline()
    override fun computation() = Schedulers.trampoline()
    override fun trampoline() = Schedulers.trampoline()
    override fun newThread() = Schedulers.trampoline()
    override fun io() = Schedulers.trampoline()
}