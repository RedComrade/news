package com.noveo.npolyakov.news.model.data.repository

import com.noveo.npolyakov.news.entity.News
import com.noveo.npolyakov.news.model.data.storage.LikesStorage
import com.noveo.npolyakov.news.model.dto.LikableNews
import io.reactivex.Single
import javax.inject.Inject


class LikableNewsRepository @Inject constructor(
        private val newsRepository: NewsRepository,
        private val likesStorage: LikesStorage) {

    fun getLikableNews(): Single<List<LikableNews>> {
        return newsRepository.getAllNews()
                .map { newsList ->
                    return@map ArrayList<LikableNews>().apply {
                        val likeIds = likesStorage.likeIds
                        newsList.forEach { news -> add(LikableNews(news, likeIds.contains(news.id))) }
                    }
                }
    }

    fun getDetailedLikableNews(newsId: String): Single<LikableNews> {
        return newsRepository.getDetailedNews(newsId)
                .map { detailedNews ->
                    val likeIds = likesStorage.likeIds
                    LikableNews(detailedNews, likeIds.contains(detailedNews.id))
                }
    }

    fun setNewsLiked(news: News, liked: Boolean) {
        val likedIdsResult =
                if (liked) likesStorage.likeIds.plus(news.id)
                else likesStorage.likeIds.minus(news.id)
        likesStorage.likeIds = likedIdsResult
    }

}