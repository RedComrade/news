package com.noveo.npolyakov.news.presentation.newsgallery

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.noveo.npolyakov.news.model.dto.LikableNews


interface GalleryView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showImages(likableNews: LikableNews)

}