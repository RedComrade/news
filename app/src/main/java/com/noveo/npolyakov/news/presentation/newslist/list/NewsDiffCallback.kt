package com.noveo.npolyakov.news.presentation.newslist.list

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil


class NewsDiffCallback(private val oldList: List<ListItem>,
                       private val newList: List<ListItem>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem is ListItem.NewsItem && newItem is ListItem.NewsItem &&
                oldItem.likableNews.news.id == newItem.likableNews.news.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem is ListItem.NewsItem && newItem is ListItem.NewsItem &&
                oldItem.likableNews == newItem.likableNews
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = oldList[oldItemPosition] as ListItem.NewsItem
        val newItem = newList[newItemPosition] as ListItem.NewsItem

        if (oldItem.likableNews.liked != newItem.likableNews.liked) {
            return Bundle().apply { putBoolean(LIKED, newItem.likableNews.liked) }
        }

        return null
    }

    companion object {
        const val LIKED = "liked"
    }
}