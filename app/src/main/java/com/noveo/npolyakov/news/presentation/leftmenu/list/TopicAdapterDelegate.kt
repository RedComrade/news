package com.noveo.npolyakov.news.presentation.leftmenu.list

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.extension.inflate
import com.noveo.npolyakov.news.model.dto.SelectableTopic
import kotlinx.android.synthetic.main.item_menu_topic.view.*


class TopicAdapterDelegate(private val clickListener: (SelectableTopic) -> Unit) :
        AbsListItemAdapterDelegate<LeftMenuItem.TopicItem, LeftMenuItem, TopicAdapterDelegate.TopicViewHolder>() {

    override fun isForViewType(itemLeft: LeftMenuItem, itemLefts: MutableList<LeftMenuItem>, position: Int) = itemLeft is LeftMenuItem.TopicItem

    override fun onCreateViewHolder(parent: ViewGroup) = TopicViewHolder(parent.inflate(R.layout.item_menu_topic), clickListener)

    override fun onBindViewHolder(itemLeft: LeftMenuItem.TopicItem, viewHolder: TopicViewHolder, payloads: MutableList<Any>) = viewHolder.bind(itemLeft.topic)


    class TopicViewHolder(private val view: View, clickListener: (SelectableTopic) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var selectableTopic: SelectableTopic

        init {
            view.setOnClickListener { clickListener.invoke(selectableTopic) }
        }

        fun bind(topic: SelectableTopic) {
            this.selectableTopic = topic

            view.menuTopicTitle.text = topic.topic.toString()
            view.setBackgroundColor(if (topic.selected) Color.WHITE else Color.LTGRAY)
        }
    }
}