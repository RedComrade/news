package com.noveo.npolyakov.news.presentation.newsdetails

import com.arellomobile.mvp.InjectViewState
import com.noveo.npolyakov.news.extension.get
import com.noveo.npolyakov.news.model.dto.ChosenNewsHolder
import com.noveo.npolyakov.news.model.interactor.news.NewsDetailsInteractor
import com.noveo.npolyakov.news.model.state.NewsDetailsModel
import com.noveo.npolyakov.news.presentation.BasePresenter
import com.noveo.npolyakov.news.presentation.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class NewsDetailsPresenter @Inject constructor(
        private val preloadedNewsHolder: ChosenNewsHolder,
        private val newsDetailsInteractor: NewsDetailsInteractor,
        private val newsDetailsModel: NewsDetailsModel,
        private val router: Router

) : BasePresenter<NewsDetailsView>() {

    override fun onFirstViewAttach() {
        withLifecycle(
                newsDetailsModel.detailedNews.subscribe { viewState.showDetailedNews(it) },
                newsDetailsModel.loading.subscribe { viewState.showLoading(it) },
                newsDetailsModel.error.subscribe { viewState.showError(it) }
        )

        preloadedNewsHolder.instance?.let {
            viewState.showPreloadedNews(it)
            newsDetailsInteractor.requestDetailedNews(it.news.id)
        }
    }

    fun onLikeClicked() {
        if (newsDetailsModel.loading.get()) return
        newsDetailsInteractor.requestToggleLikeForCurrentNews()
    }

    fun onImageClicked() {
        if (newsDetailsModel.loading.get()) return
        router.navigateTo(Screens.NewsGallery)
    }

}