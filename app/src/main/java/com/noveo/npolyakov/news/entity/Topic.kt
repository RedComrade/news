package com.noveo.npolyakov.news.entity

import com.google.gson.annotations.SerializedName


enum class Topic(private val jsonName: String) {
    @SerializedName("main")
    MAIN("main"),
    @SerializedName("policy")
    POLICY("policy"),
    @SerializedName("tech")
    TECH("tech"),
    @SerializedName("culture")
    CULTURE("culture"),
    @SerializedName("sport")
    SPORT("sport");

    override fun toString() = jsonName
}