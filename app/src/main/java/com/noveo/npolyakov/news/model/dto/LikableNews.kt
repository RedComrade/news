package com.noveo.npolyakov.news.model.dto

import com.noveo.npolyakov.news.entity.News


data class LikableNews(val news: News, val liked: Boolean)
