package com.noveo.npolyakov.news.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.noveo.npolyakov.news.extension.addTo
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


open class BasePresenter<T : MvpView> : MvpPresenter<T>() {

    private val lifecycleDisposable = CompositeDisposable()

    override fun onDestroy() {
        lifecycleDisposable.dispose()
    }

    fun withLifecycle(vararg disposables: Disposable) {
        for (dis in disposables) {
            dis.addTo(lifecycleDisposable)
        }
    }

    fun Disposable.withLifecycle() {
        this.addTo(lifecycleDisposable)
    }

    fun <T> Observable<T>.withLifecycle(onChanged: (T) -> Unit) {
        this.subscribe(onChanged)
                .addTo(lifecycleDisposable)
    }

}