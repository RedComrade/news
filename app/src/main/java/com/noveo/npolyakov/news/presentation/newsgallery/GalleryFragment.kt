package com.noveo.npolyakov.news.presentation.newsgallery

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.commit451.elasticdragdismisslayout.ElasticDragDismissListener
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.model.dto.LikableNews
import com.noveo.npolyakov.news.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_gallery.*
import toothpick.Toothpick


class GalleryFragment : BaseFragment(), GalleryView {

    override val layoutRes = R.layout.fragment_gallery

    @InjectPresenter
    lateinit var presenter: GalleryPresenter

    @ProvidePresenter
    fun providePresenter(): GalleryPresenter {
        return Toothpick.openScopes(DI.APP_SCOPE, DI.NEWS_DETAILS_SCOPE)
                .getInstance(GalleryPresenter::class.java)
    }

    override fun showImages(likableNews: LikableNews) {
        viewPager.adapter = GalleryAdapter(
                likableNews.news.imageUrlList ?: return, childFragmentManager
        )
        viewPager.setPageTransformer(true, ZoomOutPageTransformer())
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                galleryHint.text = resources.getString(R.string.news_gallery_hint, position + 1, likableNews.news.imageUrlList.size)
            }
        })
        galleryHint.text = resources.getString(R.string.news_gallery_hint, 1, likableNews.news.imageUrlList.size)
        galleryTitle.text = likableNews.news.title
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dismissibleLayout.addListener(object : ElasticDragDismissListener {
            override fun onDrag(elasticOffset: Float, elasticOffsetPixels: Float, rawOffset: Float, rawOffsetPixels: Float) {
//                Log.w("NIX", "elasticOffset: $elasticOffset, elasticOffsetPixels: $elasticOffsetPixels, rawOffset: $rawOffset, rawOffsetPixels: $rawOffsetPixels")
                val percentOfHitEdge = (TRANSPARENT_OFFSET / 100) * Math.abs(elasticOffsetPixels) / 100
                gallerySubstrate.alpha = 1 - percentOfHitEdge
                // todo: fix wrong calculation and add text view disappearing
                // todo handle situation if current image in pager is not initial image
                // todo increase velocity of dragging
            }

            override fun onDragDismissed() {
                dismissibleLayout.removeListener(this)
                activity?.supportFinishAfterTransition()
//                presenter.exit() // todo
            }
        })
    }

    inner class GalleryAdapter(private val items: List<String>, fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int) = ImageFragment.newInstance(items[position])
        override fun getCount() = items.size
    }

    companion object {
        fun newInstance() = GalleryFragment()
        const val TRANSPARENT_OFFSET = 80.0f
    }
}