package com.noveo.npolyakov.news.di.module

import android.content.Context
import com.google.gson.Gson
import com.noveo.npolyakov.news.di.provider.ApiProvider
import com.noveo.npolyakov.news.di.provider.GsonProvider
import com.noveo.npolyakov.news.di.provider.OkHttpClientProvider
import com.noveo.npolyakov.news.model.data.repository.NewsRepository
import com.noveo.npolyakov.news.model.data.server.NewsApi
import com.noveo.npolyakov.news.model.dto.ChosenNewsHolder
import com.noveo.npolyakov.news.model.state.MainModel
import com.noveo.npolyakov.news.model.state.NewsModel
import com.noveo.npolyakov.news.model.system.AppSchedulers
import com.noveo.npolyakov.news.model.system.ResourceManager
import com.noveo.npolyakov.news.model.system.SchedulersProvider
import okhttp3.OkHttpClient
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module


class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).toInstance(ResourceManager(context))

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        // Network
        bind(Gson::class.java).toProviderInstance(GsonProvider())
        bind(OkHttpClient::class.java).toProviderInstance(OkHttpClientProvider())
        bind(NewsApi::class.java).toProvider(ApiProvider::class.java).singletonInScope()

        // todo: create news list scope
        // Repository
        bind(NewsRepository::class.java).singletonInScope()
        // State
        bind(NewsModel::class.java).toInstance(NewsModel())
        bind(MainModel::class.java).toInstance(MainModel())

        bind(ChosenNewsHolder::class.java).toInstance(ChosenNewsHolder())
    }
}