package com.noveo.npolyakov.news.model.data.repository

import androidx.collection.LruCache
import com.noveo.npolyakov.news.entity.News
import com.noveo.npolyakov.news.model.data.server.NewsApi
import io.reactivex.Single
import javax.inject.Inject


class NewsRepository @Inject constructor(
        private val newsApi: NewsApi
) {

    private var cacheSize = 2 * 1024 * 1024 // 2 MiB
    private val detailedNewsCache = LruCache<String, News>(cacheSize)

    fun getAllNews() = newsApi.getAllNews()

    fun getDetailedNews(newsId: String): Single<News> {
        val cachedNews = detailedNewsCache.get(newsId)
        return cachedNews?.let { Single.just(it) }
                ?: newsApi.getNewsById(newsId).doOnSuccess { detailedNewsCache.put(it.id, it) }
    }

}