package com.noveo.npolyakov.news.presentation.leftmenu.list

import com.noveo.npolyakov.news.model.dto.SelectableTopic


sealed class LeftMenuItem {
    class TopicItem(val topic: SelectableTopic) : LeftMenuItem()
}