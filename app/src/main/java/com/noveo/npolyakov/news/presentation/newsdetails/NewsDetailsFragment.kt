package com.noveo.npolyakov.news.presentation.newsdetails

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.SharedElementCallback
import androidx.core.view.ViewCompat
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.di.module.DetailsModule
import com.noveo.npolyakov.news.extension.animateFade
import com.noveo.npolyakov.news.extension.invisible
import com.noveo.npolyakov.news.extension.snackBar
import com.noveo.npolyakov.news.model.dto.LikableNews
import com.noveo.npolyakov.news.model.system.TransitionProvider
import com.noveo.npolyakov.news.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_news_details.*
import timber.log.Timber
import toothpick.Toothpick


class NewsDetailsFragment : BaseFragment(), NewsDetailsView {

    override val layoutRes = R.layout.fragment_news_details

    @InjectPresenter
    lateinit var presenter: NewsDetailsPresenter

    @ProvidePresenter
    fun providePresenter(): NewsDetailsPresenter {
        return Toothpick.openScopes(DI.APP_SCOPE, DI.NEWS_DETAILS_SCOPE)
                .getInstance(NewsDetailsPresenter::class.java)
    }

    override fun initScope() {
        Timber.d("open details scope")
        Toothpick.openScopes(DI.APP_SCOPE, DI.NEWS_DETAILS_SCOPE)
                .apply { installModules(DetailsModule()) }
    }

    override fun closeScope() {
        Timber.d("close details scope")
        Toothpick.closeScope(DI.NEWS_DETAILS_SCOPE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
        sharedElementEnterTransition = TransitionProvider.getListToDetailsTransition(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAnimations(savedInstanceState, view)

        val behaviour = FloatingActionButton.Behavior().apply { isAutoHideEnabled = false }
        val fabLayoutParams = likeFab.layoutParams as CoordinatorLayout.LayoutParams
        fabLayoutParams.behavior = behaviour
        likeFab.setOnClickListener { presenter.onLikeClicked() }
        detailsNewsImage.setOnClickListener { presenter.onImageClicked() }
    }

    override fun showPreloadedNews(preloadedNews: LikableNews) {
        ViewCompat.setTransitionName(detailsNewsImage, preloadedNews.news.imageUrl)
        Glide.with(this)
                .load(preloadedNews.news.imageUrl)
                .apply(RequestOptions()
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .onlyRetrieveFromCache(true)
                )
                .into(detailsNewsImage)
        newsTitle.text = preloadedNews.news.title
        likeFab.isChecked = preloadedNews.liked
    }

    override fun showDetailedNews(detailedNews: LikableNews) {
        newsDescription.text = detailedNews.news.html
        likeFab.isChecked = detailedNews.liked
    }

    override fun showLoading(loading: Boolean) {
        swipeRefresh.isRefreshing = loading
    }

    override fun showError(errorMessage: String) {
        snackBar("Error")
    }

    fun getPreviewImageView(): View {
        return detailsNewsImage
    }

    private fun setupAnimations(savedInstanceState: Bundle?, view: View?) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return

        if (savedInstanceState == null) { // animate only on first creation
            likeFab.invisible()
            newsTitle.invisible()
            setEnterSharedElementCallback(object : SharedElementCallback() {
                override fun onSharedElementEnd(sharedElementNames: MutableList<String>?,
                                                sharedElements: MutableList<View>?,
                                                sharedElementSnapshots: MutableList<View>?) {
                    view?.postDelayed({
                        likeFab?.show()
                        newsTitle?.animateFade(false)
                    }, REVEAL_DELAY)
                }
            })
        }
    }

    companion object {
        fun newInstance() = NewsDetailsFragment()
        const val REVEAL_DELAY: Long = 300
    }
}