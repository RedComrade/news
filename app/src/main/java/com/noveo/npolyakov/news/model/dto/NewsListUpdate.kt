package com.noveo.npolyakov.news.model.dto

import androidx.recyclerview.widget.DiffUtil
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem
import com.noveo.npolyakov.news.presentation.newslist.list.NewsDiffCallback


data class NewsListUpdate(
        val newsList: List<ListItem>,
        val diffResult: DiffUtil.DiffResult
) {
    companion object {
        fun calculate(oldList: List<ListItem>, newList: List<ListItem>): NewsListUpdate =
                NewsListUpdate(newList, DiffUtil.calculateDiff(NewsDiffCallback(oldList, newList)))
    }
}