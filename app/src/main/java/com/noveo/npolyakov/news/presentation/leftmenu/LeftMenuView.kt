package com.noveo.npolyakov.news.presentation.leftmenu

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.noveo.npolyakov.news.presentation.leftmenu.list.LeftMenuItem


interface LeftMenuView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setTopics(topics: List<LeftMenuItem>)

}