package com.noveo.npolyakov.news.model.dto

import com.noveo.npolyakov.news.entity.Topic


data class SelectableTopic(
        val topic: Topic,
        val selected: Boolean
)