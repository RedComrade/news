package com.noveo.npolyakov.news.presentation.newslist

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.noveo.npolyakov.news.model.dto.NewsListUpdate


interface NewsListView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateNewsList(update: NewsListUpdate)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(error: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading(show: Boolean)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showEmptyList(show: Boolean)

}