package com.noveo.npolyakov.news.presentation.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.presentation.BaseFragment
import com.noveo.npolyakov.news.presentation.Screens
import com.noveo.npolyakov.news.presentation.newsdetails.NewsDetailsFragment
import com.noveo.npolyakov.news.presentation.newslist.NewsListFragment
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace
import toothpick.Toothpick
import javax.inject.Inject


class MainActivity : MvpAppCompatActivity(), MainView {

    private lateinit var drawerController: DrawerController

    @Inject
    lateinit var navigationHolder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter {
        return Toothpick
                .openScope(DI.APP_SCOPE)
                .getInstance(MainPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.openScopes(DI.APP_SCOPE).apply {
            Toothpick.inject(this@MainActivity, this)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawerController = DrawerController(drawerLayout, supportFragmentManager, R.id.navigationDrawer)

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf(Replace(Screens.NewsList)))
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
        if (fragment !is BaseFragment || !fragment.interceptBackPressed()) {
            presenter.onBackPressed()
        }
    }

    override fun setMenuOpened(opened: Boolean) {
        drawerController.setDrawerOpened(opened)
    }


    private val navigator = object : SupportAppNavigator(this, R.id.mainContainer) {
        override fun createStartActivityOptions(command: Command?, activityIntent: Intent?): Bundle? {
            return if (command is Forward && activityIntent?.extras?.getString(Screens.SCREEN_KEY, "") == Screens.NEWS_GALLERY) {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
                val previewImage = (currentFragment as? NewsDetailsFragment)?.getPreviewImageView()
                        ?: return null
                val transitionName = ViewCompat.getTransitionName(previewImage) ?: return null
                ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity, previewImage, transitionName).toBundle()
            } else null
        }

        override fun applyCommand(command: Command?) {
            super.applyCommand(command)
            drawerController.updateNavDrawer()
            // todo disable swipe to refresh for details screen
            // swiperefreshLayout.setEnabled(true);
            // swiperefreshLayout.setRefreshing(true);
        }

        override fun setupFragmentTransaction(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction?) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
            if (fragmentTransaction == null) return

            if (command is Forward && currentFragment is NewsListFragment && nextFragment is NewsDetailsFragment) {
                val chosenImage = currentFragment.chosenImage
                val transitionName = ViewCompat.getTransitionName(chosenImage) ?: return
                fragmentTransaction.addSharedElement(chosenImage, transitionName)
            }
        }
    }

    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}
