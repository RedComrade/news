package com.noveo.npolyakov.news.model.data.storage

import android.content.Context
import javax.inject.Inject


class LikesStorage @Inject constructor(private val context: Context) {

    var likeIds: Set<String>
        get() = getSharedPreferences(FAVORITES_DATA).getStringSet(FAVORITES_KEY, null) ?: HashSet()
        set(value) {
            getSharedPreferences(FAVORITES_DATA).edit().putStringSet(FAVORITES_KEY, value).apply()
        }

    private fun getSharedPreferences(prefsName: String) = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    companion object {
        const val FAVORITES_DATA = "favorites_storage"
        const val FAVORITES_KEY = "favorites_key"
    }
}
