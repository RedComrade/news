package com.noveo.npolyakov.news.model.system

import android.content.Context
import android.os.Build
import android.transition.Transition
import android.transition.TransitionInflater


class TransitionProvider {
    companion object {
        fun getListToDetailsTransition(context: Context): Transition? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
            else null
        }

        fun getDetailsToGalleryTransition(context: Context): Transition? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
            else null
        }
    }
}