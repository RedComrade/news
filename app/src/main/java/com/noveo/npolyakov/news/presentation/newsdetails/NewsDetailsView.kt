package com.noveo.npolyakov.news.presentation.newsdetails

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.noveo.npolyakov.news.model.dto.LikableNews


interface NewsDetailsView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPreloadedNews(preloadedNews: LikableNews)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showDetailedNews(detailedNews: LikableNews)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading(loading: Boolean)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(errorMessage: String)

}