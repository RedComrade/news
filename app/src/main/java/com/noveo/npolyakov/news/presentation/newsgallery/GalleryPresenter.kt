package com.noveo.npolyakov.news.presentation.newsgallery

import com.arellomobile.mvp.InjectViewState
import com.noveo.npolyakov.news.extension.get
import com.noveo.npolyakov.news.model.state.NewsDetailsModel
import com.noveo.npolyakov.news.presentation.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class GalleryPresenter @Inject constructor(
        private val newsDetailsModel: NewsDetailsModel,
        private val router: Router

) : BasePresenter<GalleryView>() {

    override fun onFirstViewAttach() {
        viewState.showImages(newsDetailsModel.detailedNews.get())
    }

    fun exit() {
        router.exit()
    }

}