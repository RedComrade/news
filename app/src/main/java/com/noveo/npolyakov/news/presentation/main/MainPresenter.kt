package com.noveo.npolyakov.news.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.noveo.npolyakov.news.model.state.MainModel
import com.noveo.npolyakov.news.presentation.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class MainPresenter @Inject constructor(
        private val mainModel: MainModel,
        private val router: Router
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        withLifecycle(
                mainModel.menuOpenBus.subscribe { viewState.setMenuOpened(it) }
        )
    }

    fun onBackPressed() = router.exit()

}