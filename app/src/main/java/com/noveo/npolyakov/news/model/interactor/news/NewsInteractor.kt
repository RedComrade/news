package com.noveo.npolyakov.news.model.interactor.news

import com.noveo.npolyakov.news.entity.Topic
import com.noveo.npolyakov.news.extension.userMessage
import com.noveo.npolyakov.news.model.data.extension.deepCopy
import com.noveo.npolyakov.news.model.data.extension.toListItems
import com.noveo.npolyakov.news.model.data.repository.LikableNewsRepository
import com.noveo.npolyakov.news.model.dto.NewsListUpdate
import com.noveo.npolyakov.news.model.state.MainModel
import com.noveo.npolyakov.news.model.state.NewsModel
import com.noveo.npolyakov.news.model.system.ResourceManager
import com.noveo.npolyakov.news.model.system.SchedulersProvider
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


class NewsInteractor @Inject constructor(
        private val resourceManager: ResourceManager,
        private val likableNewsRepository: LikableNewsRepository,
        private val schedulers: SchedulersProvider,
        private val newsModel: NewsModel,
        private val mainModel: MainModel
) {

    private val listUpdateTransformer = createUpdateResultTransformer()
    private val loadingStateTransformer = createLoadingStateTransformer()
    private val topicFilterTransformer = createNewsByTopicTransformer()

    fun requestFreshNews(): Disposable {
        return intentToUpdateNewsList(
                likableNewsRepository.getLikableNews()
                        .subscribeOn(schedulers.io())
                        .map { it.toListItems() }
        )
    }

    fun requestToChangeTopic(topic: Topic): Disposable {
        mainModel.topic.accept(topic)
        return intentToUpdateNewsList(
                Single.just(newsModel.fullList.value)
        )
    }

    fun requstSorting(sortBy: SortBy): Disposable {
        return intentToUpdateNewsList(
                Single.fromCallable { newsModel.fullList.value }
                        .map { newsList -> newsList.sortedWith(sortBy.comparator) })
    }

    fun intentToUpdateNewsList(intentionSingle: Single<List<ListItem>>): Disposable {
        return intentionSingle
                .doOnSuccess { newsModel.fullList.accept(it) }
                .compose(topicFilterTransformer)
                .compose(listUpdateTransformer)
                .observeOn(schedulers.ui())
                .compose(loadingStateTransformer)
                .subscribe({ newsModel.listUpdate.accept(it) },
                        {
                            newsModel.errors.accept(it.userMessage(resourceManager))
                            Timber.w(it, "requestFreshNews error: ")
                        })
    }

    /**
     * transforms single observable of news items to update result entity for presentation layer
     * it saves previous fullList as deep copy for calculate diffs
     */
    private fun createUpdateResultTransformer() = SingleTransformer<List<ListItem>, NewsListUpdate> { upstream ->
        upstream.map { newValue ->
            val result = Pair(lastValue, newValue)
            lastValue = newValue.deepCopy()
            result
        }
                .observeOn(schedulers.computation())
                .map { (first, second) -> NewsListUpdate.calculate(first, second) }
    }

    private fun createLoadingStateTransformer() = SingleTransformer<NewsListUpdate, NewsListUpdate> { upstream ->
        upstream.doOnSubscribe { newsModel.loading.accept(true) }
                .doAfterTerminate { newsModel.loading.accept(false) }
                .doOnDispose { newsModel.loading.accept(false) }
    }

    private fun createNewsByTopicTransformer() = SingleTransformer<List<ListItem>, List<ListItem>> { upstream ->
        upstream.map { newsList ->
            newsList.filter { item ->
                if (item !is ListItem.NewsItem) return@filter true
                item.likableNews.news.topics?.contains(mainModel.topic.value) ?: false
            }
        }
    }

    enum class SortBy(val comparator: Comparator<ListItem>) {
        NAME(Comparators.NAME),
        DATE(Comparators.DATE);
    }

    companion object {
        private var lastValue: List<ListItem> = listOf()
    }
}