package com.noveo.npolyakov.news.presentation.newsgallery

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.extension.visible
import com.noveo.npolyakov.news.extension.withArgs
import com.noveo.npolyakov.news.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_image.*


class ImageFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_image

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val imageUrl = requireNotNull(arguments).getString(URL_KEY)
        ViewCompat.setTransitionName(image, imageUrl)

        progressBar.visible(true)
        Glide.with(this)
                .load(imageUrl)
                .apply(RequestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.visible(false)
                        activity?.supportStartPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        progressBar.visible(false)
                        activity?.supportStartPostponedEnterTransition()
                        return false
                    }
                })
                .into(image)
    }

    companion object {
        private const val URL_KEY = "url_key"
        fun newInstance(imageUrl: String) =
                ImageFragment().withArgs { it.putString(URL_KEY, imageUrl) }
    }
}