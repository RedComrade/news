package com.noveo.npolyakov.news.model.state

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.noveo.npolyakov.news.model.dto.NewsListUpdate
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem


class NewsModel {

    val loading: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(false)
    val fullList: BehaviorRelay<List<ListItem>> = BehaviorRelay.create<List<ListItem>>()
    val listUpdate: BehaviorRelay<NewsListUpdate> = BehaviorRelay.create<NewsListUpdate>()
    val errors: PublishRelay<String> = PublishRelay.create<String>()

}