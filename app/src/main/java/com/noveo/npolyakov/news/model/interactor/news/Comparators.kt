package com.noveo.npolyakov.news.model.interactor.news

import com.noveo.npolyakov.news.presentation.newslist.list.ListItem


enum class Comparators : Comparator<ListItem> {

    NAME {
        override fun compare(first: ListItem?, second: ListItem?): Int {
            if (first == null || second == null) return 0
            if (first !is ListItem.NewsItem || second !is ListItem.NewsItem) return 0
            return first.likableNews.news.title.compareTo(second.likableNews.news.title)
        }
    },

    DATE {
        override fun compare(first: ListItem?, second: ListItem?): Int {
            if (first == null || second == null) return 0
            if (first !is ListItem.NewsItem || second !is ListItem.NewsItem) return 0
            val l1 = first.likableNews.news.publishDateTime.millis
            val l2 = second.likableNews.news.publishDateTime.millis
            return (l1 - l2).toInt()
        }
    }

}