package com.noveo.npolyakov.news.presentation.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import com.google.android.material.floatingactionbutton.FloatingActionButton


class CheckableFab : FloatingActionButton, Checkable {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var stateChecked = false

    override fun isChecked(): Boolean {
        return stateChecked
    }

    override fun setChecked(isChecked: Boolean) {
        if (stateChecked != isChecked) {
            stateChecked = isChecked
            refreshDrawableState()
        }
    }

    override fun toggle() {
        stateChecked = !stateChecked
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 1)
        if (stateChecked) {
            View.mergeDrawableStates(drawableState, CHECKED_STATE_SET)
        }
        return drawableState
    }

    companion object {
        val CHECKED_STATE_SET = intArrayOf(android.R.attr.state_checked)
    }
}