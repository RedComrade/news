package com.noveo.npolyakov.news.entity

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime


data class News(
        @SerializedName("id") val id: String,
        @SerializedName("image") val imageUrl: String?,
        @SerializedName("images") val imageUrlList: List<String>?,
        @SerializedName("pubDate") val publishDateTime: DateTime,
        @SerializedName("title") val title: String,
        @SerializedName("topics") val topics: List<Topic>? = null,
        @SerializedName("html") val html: String?
)