package com.noveo.npolyakov.news.presentation.newslist.list

import com.noveo.npolyakov.news.model.dto.LikableNews


sealed class ListItem {
    class NewsItem(val likableNews: LikableNews) : ListItem()

}