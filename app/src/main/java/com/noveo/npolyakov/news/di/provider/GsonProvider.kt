package com.noveo.npolyakov.news.di.provider

import android.text.TextUtils
import com.google.gson.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.lang.reflect.Type
import java.util.*
import javax.inject.Provider


class GsonProvider : Provider<Gson> {

    override fun get(): Gson {
        return GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(DateTime::class.java, JodaDateTimeSerializer(DEFAULT_TIME_FORMAT))
                .create()
    }

    class JodaDateTimeSerializer(pattern: String) : JsonDeserializer<DateTime>, JsonSerializer<DateTime> {
        private val formatter: DateTimeFormatter = DateTimeFormat.forPattern(pattern).withLocale(Locale.ENGLISH)

        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement?, typeOfT: Type, context: JsonDeserializationContext): DateTime? {
            return if (json != null && !TextUtils.isEmpty(json.asString)) {
                formatter.parseDateTime(json.asString)
            } else null
        }

        override fun serialize(src: DateTime?, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
            return if (src != null) {
                JsonPrimitive(formatter.print(src))
            } else JsonPrimitive("")
        }
    }

    companion object {
        private const val DEFAULT_TIME_FORMAT = "dd MMM yyyy HH:mm:ss Z"
    }
}