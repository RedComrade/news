package com.noveo.npolyakov.news.model.data.extension

import com.noveo.npolyakov.news.entity.News
import com.noveo.npolyakov.news.model.dto.LikableNews
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem


fun News.deepCopy() =
        News(this.id, this.imageUrl, this.imageUrlList,
                this.publishDateTime, this.title,
                this.topics, this.html)

fun LikableNews.deepCopy() = LikableNews(news.deepCopy(), liked)

fun List<ListItem>.deepCopy() = this.map { item ->
    when (item) {
        is ListItem.NewsItem -> ListItem.NewsItem(item.likableNews.deepCopy())
    }
}

fun List<LikableNews>.toListItems() = this.map { news -> ListItem.NewsItem(news) }