package com.noveo.npolyakov.news.model.state

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.noveo.npolyakov.news.model.dto.LikableNews


class NewsDetailsModel {

    val detailedNews: BehaviorRelay<LikableNews> = BehaviorRelay.create<LikableNews>()
    val loading: BehaviorRelay<Boolean> = BehaviorRelay.create<Boolean>()
    val error: PublishRelay<String> = PublishRelay.create<String>()

}