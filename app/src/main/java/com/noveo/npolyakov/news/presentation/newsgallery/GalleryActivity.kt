package com.noveo.npolyakov.news.presentation.newsgallery

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.noveo.npolyakov.news.R


class GalleryActivity : MvpAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        if (savedInstanceState == null) {
            supportPostponeEnterTransition()
            supportFragmentManager.beginTransaction()
                    .add(R.id.galleryContainer, GalleryFragment.newInstance())
                    .commit()
        }
    }

}