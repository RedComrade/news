package com.noveo.npolyakov.news.presentation.newslist

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.extension.snackBar
import com.noveo.npolyakov.news.extension.visible
import com.noveo.npolyakov.news.model.dto.NewsListUpdate
import com.noveo.npolyakov.news.presentation.BaseFragment
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem
import com.noveo.npolyakov.news.presentation.newslist.list.NewsAdapterDelegate
import kotlinx.android.synthetic.main.fragment_news_list.*
import toothpick.Toothpick


class NewsListFragment : BaseFragment(), NewsListView {

    override val layoutRes = R.layout.fragment_news_list
    private val newsAdapter = NewsAdapter()
    lateinit var chosenImage: View
        private set

    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    @ProvidePresenter
    fun providePresenter(): NewsListPresenter = Toothpick
            .openScope(DI.APP_SCOPE)
            .getInstance(NewsListPresenter::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        newsRecycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = this@NewsListFragment.newsAdapter
            addItemDecoration(
                    DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                            .apply {
                                ContextCompat.getDrawable(context, R.drawable.news_list_divider)?.let {
                                    setDrawable(it)
                                }
                            })
        }

        swipeRefresh.apply {
            setOnRefreshListener { presenter.onRefreshTriggered() }
            setColorSchemeResources(R.color.accent)
        }
    }

    override fun updateNewsList(update: NewsListUpdate) {
        newsAdapter.setData(update.newsList)
        update.diffResult.dispatchUpdatesTo(newsAdapter)
    }

    override fun showError(error: String) {
        snackBar(error).show()
    }

    override fun showLoading(show: Boolean) {
        swipeRefresh.isRefreshing = show
    }

    override fun showEmptyList(show: Boolean) {
        newsEmpty.visible(show)
    }

    inner class NewsAdapter : ListDelegationAdapter<MutableList<ListItem>>() {

        private var newsAdapterDelegate: NewsAdapterDelegate

        init {
            items = mutableListOf()
            delegatesManager.apply {
                newsAdapterDelegate = NewsAdapterDelegate { news, imageView ->
                    chosenImage = imageView
                    presenter.onNewsClicked(news)
                }
                addDelegate(newsAdapterDelegate)
            }
        }

        fun setData(list: List<ListItem>) {
            items = list.toMutableList()
        }
    }

    companion object {
        fun newInstance() = NewsListFragment()
    }
}