package com.noveo.npolyakov.news.presentation.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


interface MainView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun setMenuOpened(opened: Boolean)

}