package com.noveo.npolyakov.news.model.state

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.noveo.npolyakov.news.entity.Topic


class MainModel {

    val topic: BehaviorRelay<Topic> = BehaviorRelay.createDefault(Topic.MAIN)
    val menuOpenBus: PublishRelay<Boolean> = PublishRelay.create<Boolean>()

}