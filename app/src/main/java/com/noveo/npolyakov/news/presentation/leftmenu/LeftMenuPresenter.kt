package com.noveo.npolyakov.news.presentation.leftmenu

import com.arellomobile.mvp.InjectViewState
import com.noveo.npolyakov.news.entity.Topic
import com.noveo.npolyakov.news.model.dto.SelectableTopic
import com.noveo.npolyakov.news.model.interactor.news.NewsInteractor
import com.noveo.npolyakov.news.model.state.MainModel
import com.noveo.npolyakov.news.presentation.BasePresenter
import com.noveo.npolyakov.news.presentation.leftmenu.list.LeftMenuItem
import javax.inject.Inject


@InjectViewState
class LeftMenuPresenter @Inject constructor(
        private val mainModel: MainModel,
        private val newsInteractor: NewsInteractor
) : BasePresenter<LeftMenuView>() {

    override fun onFirstViewAttach() {
        mainModel.topic.withLifecycle {
            val menuList = getMenuListSelected(it)
            viewState.setTopics(menuList)
        }
    }

    fun onTopicClicked(topic: Topic) {
        newsInteractor.requestToChangeTopic(topic)
        mainModel.menuOpenBus.accept(false)
    }

    private fun getMenuListSelected(selectedTopic: Topic): List<LeftMenuItem.TopicItem> {
        return Topic.values()
                .map { LeftMenuItem.TopicItem(SelectableTopic(it, it == selectedTopic)) }
    }

}