package com.noveo.npolyakov.news.presentation

import android.content.Context
import android.content.Intent
import com.noveo.npolyakov.news.presentation.newsdetails.NewsDetailsFragment
import com.noveo.npolyakov.news.presentation.newsgallery.GalleryActivity
import com.noveo.npolyakov.news.presentation.newslist.NewsListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen


object Screens {

    object NewsList : SupportAppScreen() {
        override fun getFragment() = NewsListFragment.newInstance()
    }

    object NewsDetails : SupportAppScreen() {
        override fun getFragment() = NewsDetailsFragment.newInstance()
    }

    object NewsGallery : SupportAppScreen() {
        override fun getActivityIntent(context: Context): Intent {
            return Intent(context, GalleryActivity::class.java).putExtra(SCREEN_KEY, NEWS_GALLERY)
        }
    }

    const val SCREEN_KEY = "screen"
    const val NEWS_GALLERY = "news_gallery"
}