package com.noveo.npolyakov.news.di.module

import com.noveo.npolyakov.news.model.state.NewsDetailsModel
import toothpick.config.Module


class DetailsModule : Module() {

    init {
        bind(NewsDetailsModel::class.java).toInstance(NewsDetailsModel())
    }

}