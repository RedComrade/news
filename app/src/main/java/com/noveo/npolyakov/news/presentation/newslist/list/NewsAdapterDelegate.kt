package com.noveo.npolyakov.news.presentation.newslist.list

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.extension.inflate
import com.noveo.npolyakov.news.model.dto.LikableNews
import kotlinx.android.synthetic.main.item_news_preview.view.*


class NewsAdapterDelegate(private val clickListener: (LikableNews, View) -> Unit) :
        AbsListItemAdapterDelegate<ListItem.NewsItem, ListItem, NewsAdapterDelegate.NewsViewHolder>() {

    override fun isForViewType(item: ListItem, items: MutableList<ListItem>, position: Int) =
            item is ListItem.NewsItem

    override fun onBindViewHolder(item: ListItem.NewsItem, viewHolder: NewsViewHolder, payloads: MutableList<Any>) =
            viewHolder.bind(item.likableNews, payloads)

    override fun onCreateViewHolder(parent: ViewGroup) =
            NewsViewHolder(parent.inflate(R.layout.item_news_preview), clickListener)


    class NewsViewHolder(private val view: View,
                         private val clickListener: (LikableNews, ImageView) -> Unit)
        : RecyclerView.ViewHolder(view) {

        private lateinit var likableNews: LikableNews

        fun bind(likableNews: LikableNews, payloads: MutableList<Any>) {
            this.likableNews = likableNews
            view.setOnClickListener { clickListener.invoke(likableNews, view.newsPreviewImage) }
            ViewCompat.setTransitionName(view.newsPreviewImage, likableNews.news.imageUrl)
            likableNews.news.title.let { view.newsTitle.text = it }
            likableNews.news.topics.let { view.newsTopics.text = it.toString() }

            Glide.with(view.context)
                    .load(likableNews.news.imageUrl ?: "")
                    .apply(RequestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
                    .into(view.newsPreviewImage)

            setLiked(
                    if (payloads.isEmpty()) likableNews.liked
                    else (payloads[0] as Bundle).getBoolean(NewsDiffCallback.LIKED)
            )
        }

        private fun setLiked(liked: Boolean) {
            view.newsLikeImage.setImageResource(if (liked) R.drawable.ic_heart_full_56dp else R.drawable.ic_heart_empty_56dp)
        }
    }
}