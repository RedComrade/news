package com.noveo.npolyakov.news.model.data.server

import com.noveo.npolyakov.news.entity.News
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface NewsApi {

    @GET("/news/getAll")
    fun getAllNews(): Single<List<News>>

    @GET("/news/getById")
    fun getNewsById(@Query("id") id: String): Single<News>
}
