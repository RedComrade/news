package com.noveo.npolyakov.news.model.interactor.news

import com.noveo.npolyakov.news.extension.get
import com.noveo.npolyakov.news.extension.userMessage
import com.noveo.npolyakov.news.model.data.repository.LikableNewsRepository
import com.noveo.npolyakov.news.model.dto.LikableNews
import com.noveo.npolyakov.news.model.state.NewsDetailsModel
import com.noveo.npolyakov.news.model.state.NewsModel
import com.noveo.npolyakov.news.model.system.ResourceManager
import com.noveo.npolyakov.news.model.system.SchedulersProvider
import com.noveo.npolyakov.news.presentation.newslist.list.ListItem
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


class NewsDetailsInteractor @Inject constructor(
        private val likableNewsRepository: LikableNewsRepository,
        private val schedulers: SchedulersProvider,
        private val newsDetailsModel: NewsDetailsModel,
        private val newsModel: NewsModel,
        private val newsInteractor: NewsInteractor,
        private val resourceManager: ResourceManager
) {

    private val loadingTransformer = createLoadingStateTransformer()

    fun requestDetailedNews(newsId: String): Disposable {
        return likableNewsRepository.getDetailedLikableNews(newsId)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .compose(loadingTransformer)
                .subscribe(
                        { newsDetailsModel.detailedNews.accept(it) },
                        {
                            Timber.w(it, "requestDetailedNews error: ")
                            newsDetailsModel.error.accept(it.userMessage(resourceManager))
                        })
    }

    fun requestToggleLikeForCurrentNews() {
        val currentNews = newsDetailsModel.detailedNews.get()
        likableNewsRepository.setNewsLiked(currentNews.news, !currentNews.liked)
        val toggledNews = LikableNews(currentNews.news.copy(), !currentNews.liked)
        newsDetailsModel.detailedNews.accept(toggledNews)

        val changedNewsList = newsModel.fullList.get().map {
            if (it is ListItem.NewsItem && it.likableNews.news.id == currentNews.news.id)
                ListItem.NewsItem(LikableNews(it.likableNews.news, toggledNews.liked))
            else it
        }

        newsModel.fullList.accept(changedNewsList)
        newsInteractor.intentToUpdateNewsList(Single.just(newsModel.fullList.value))
    }

    private fun createLoadingStateTransformer() = SingleTransformer<LikableNews, LikableNews> { upstream ->
        upstream.doOnSubscribe { newsDetailsModel.loading.accept(true) }
                .doAfterTerminate { newsDetailsModel.loading.accept(false) }
                .doOnDispose { newsDetailsModel.loading.accept(false) }
    }

}