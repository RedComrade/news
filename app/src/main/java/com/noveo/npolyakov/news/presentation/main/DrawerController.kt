package com.noveo.npolyakov.news.presentation.main

import androidx.annotation.IdRes
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.presentation.leftmenu.LeftMenuFragment


class DrawerController(private val drawerLayout: DrawerLayout,
                       private val supportFragmentManager: FragmentManager,
                       @IdRes private val drawerId: Int) {

    fun setDrawerOpened(open: Boolean) {
        drawerLayout.post {
            if (open) drawerLayout.openDrawer(GravityCompat.START)
            else drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    fun setDrawerLocked(enable: Boolean) {
        drawerLayout.setDrawerLockMode(
                if (enable) DrawerLayout.LOCK_MODE_UNLOCKED
                else DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
                GravityCompat.START
        )
    }

    fun updateNavDrawer() {
        supportFragmentManager.executePendingTransactions()

        val drawerFragment = supportFragmentManager.findFragmentById(drawerId) as LeftMenuFragment
        supportFragmentManager.findFragmentById(R.id.mainContainer)?.let {
            when (it) {
                // todo
//                is MainFragment -> drawerFragment.onScreenChanged(NavigationDrawerView.LeftMenuItem.ACTIVITY)
            }
            setDrawerLocked(isNavDrawerAvailableForFragment(it))
        }
    }

    private fun isNavDrawerAvailableForFragment(currentFragment: Fragment) = when (currentFragment) {
        is Fragment -> true // todo
        else -> true
    }

}