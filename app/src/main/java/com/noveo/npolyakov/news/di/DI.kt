package com.noveo.npolyakov.news.di


object DI {
    const val APP_SCOPE = "app_scope"
    const val NEWS_DETAILS_SCOPE = "news_details_scope"
}