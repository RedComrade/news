package com.noveo.npolyakov.news.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.noveo.npolyakov.news.App


private const val STATE_LAUNCH_FLAG = "state_launch_flag"
private const val STATE_SCOPE_WAS_CLOSED = "state_scope_was_closed"

abstract class BaseFragment : MvpAppCompatFragment() {

    abstract val layoutRes: Int
    private var instanceStateSaved: Boolean = false

    open fun interceptBackPressed() = false
    open fun initScope() {}
    open fun closeScope() {}

    // just for handle nullability
    override fun getContext(): Context {
        return requireNotNull(super.getContext()) { "Fragment [$this] doesn't associated with a context." }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val savedAppCode = savedInstanceState?.getString(STATE_LAUNCH_FLAG)
        //False - if fragment was restored without new app process (for example: activity rotation)
        val isNewInAppProcess = savedAppCode != App.appCode
        val scopeWasClosed = savedInstanceState?.getBoolean(STATE_SCOPE_WAS_CLOSED) ?: true
        val scopeIsNotInit = isNewInAppProcess || scopeWasClosed
        if (scopeIsNotInit) {
            initScope()
        }

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onResume() {
        super.onResume()
        instanceStateSaved = false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
        outState.putString(STATE_LAUNCH_FLAG, App.appCode)
        outState.putBoolean(STATE_SCOPE_WAS_CLOSED, needCloseScope()) //save it but will be used only if destroyed
    }

    override fun onDestroy() {
        super.onDestroy()
        if (needCloseScope()) {
            closeScope()
        }
    }

    //It will be valid only for 'onDestroy()' method
    private fun needCloseScope(): Boolean =
            when {
                activity?.isChangingConfigurations == true -> false
                activity?.isFinishing == true -> true
                else -> isRealRemoving()
            }

    //This is android, baby!
    private fun isRealRemoving(): Boolean =
            //because isRemoving == true for fragment in backstack on screen rotation
            (isRemoving && !instanceStateSaved)
                    || ((parentFragment as? BaseFragment)?.isRealRemoving() ?: false)

}
