package com.noveo.npolyakov.news.presentation.newslist

import com.arellomobile.mvp.InjectViewState
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.extension.get
import com.noveo.npolyakov.news.model.dto.ChosenNewsHolder
import com.noveo.npolyakov.news.model.dto.LikableNews
import com.noveo.npolyakov.news.model.interactor.news.NewsInteractor
import com.noveo.npolyakov.news.model.state.NewsModel
import com.noveo.npolyakov.news.presentation.BasePresenter
import com.noveo.npolyakov.news.presentation.Screens
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject


@InjectViewState
class NewsListPresenter @Inject constructor(
        private val router: Router,
        private val newsInteractor: NewsInteractor,
        private val newsModel: NewsModel
) : BasePresenter<NewsListView>() {

    override fun onFirstViewAttach() {
        withLifecycle(
                newsModel.loading.subscribe { viewState.showLoading(it) },
                newsModel.errors.subscribe { viewState.showError(it) },
                newsModel.listUpdate.subscribe {
                    viewState.showEmptyList(it.newsList.isEmpty())
                    viewState.updateNewsList(it)
                }
        )
        viewState.showEmptyList(true)
        requestNews()
    }

    internal fun onRefreshTriggered() {
        requestNews()
    }

    private fun requestNews() {
        if (!newsModel.loading.get()) {
            Timber.d("request news")
            newsInteractor.requestFreshNews().withLifecycle()
        }
    }

    fun onNewsClicked(likableNews: LikableNews) {
        if (newsModel.loading.get()) return
        Toothpick.openScopes(DI.APP_SCOPE).getInstance(ChosenNewsHolder::class.java).instance = likableNews
        router.navigateTo(Screens.NewsDetails)
    }

}