package com.noveo.npolyakov.news

import android.app.Application
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.di.module.AppModule
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import java.util.*


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appCode = UUID.randomUUID().toString()

        initLogger()
        initJodaTime()
        initAppScope()
        initToothpick()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initJodaTime() {
        JodaTimeAndroid.init(this)
    }

    private fun initAppScope() {
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    companion object {
        lateinit var appCode: String
            private set
    }
}