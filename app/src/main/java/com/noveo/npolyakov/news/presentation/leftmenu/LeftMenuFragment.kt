package com.noveo.npolyakov.news.presentation.leftmenu

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.noveo.npolyakov.news.R
import com.noveo.npolyakov.news.di.DI
import com.noveo.npolyakov.news.presentation.BaseFragment
import com.noveo.npolyakov.news.presentation.leftmenu.list.LeftMenuItem
import com.noveo.npolyakov.news.presentation.leftmenu.list.TopicAdapterDelegate
import kotlinx.android.synthetic.main.fragment_nav_drawer.*
import toothpick.Toothpick


class LeftMenuFragment : BaseFragment(), LeftMenuView {

    override val layoutRes = R.layout.fragment_nav_drawer

    private val menuAdapter = MenuAdapter()

    @InjectPresenter
    lateinit var presenter: LeftMenuPresenter

    @ProvidePresenter
    fun providePresenter(): LeftMenuPresenter = Toothpick
            .openScope(DI.APP_SCOPE)
            .getInstance(LeftMenuPresenter::class.java)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menuRecycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            adapter = this@LeftMenuFragment.menuAdapter
        }
    }

    override fun setTopics(topics: List<LeftMenuItem>) {
        menuAdapter.setData(topics)
    }


    inner class MenuAdapter : ListDelegationAdapter<MutableList<LeftMenuItem>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(TopicAdapterDelegate { presenter.onTopicClicked(it.topic) })
        }

        fun setData(list: List<LeftMenuItem>) {
            items.clear()
            items.addAll(list)
            notifyDataSetChanged()
        }
    }
}
